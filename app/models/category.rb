class Category < ActiveRecord::Base
	validates :name, presence: true,
                    length: { minimum: 5 }
	validates :description, presence: true,
                    length: { minimum: 5 }
     
    validates :hash_code, presence: true,
    				length: { is: 32 }              
	has_many :words, dependent: :destroy
end
