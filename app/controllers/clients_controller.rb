class ClientsController < ApplicationController
	skip_before_action :require_login
	
	def show
	  	@category = Category.where(hash_code: params[:id]).first

	  	connection = ActiveRecord::Base.connection()
	  	@words = connection.execute("select word from words where category_id =" + @category.id.to_s + " ORDER BY RAND() LIMIT 150")
	  	@count = @words.size
	  	respond_to do |format|
	  		format.html
	  		format.xml
	  	end
	end

	def index
		@categories = Category.all

		respond_to do |format|
	  		format.html
	  		format.xml
	  	end
	end
	
end
