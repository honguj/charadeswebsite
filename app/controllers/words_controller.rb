class WordsController < ApplicationController

	def new
		@word = Word.new
	end

	def create
		@word = Word.new(word_params)
		@word.category_id = params[:category_id]
 		if @word.save
			redirect_to category_path(@word.category_id)
		else
			render 'new'
		end
	end

	def edit
	  	@word = Word.find(params[:id])
	end

	def update
		@word = Word.find(params[:id])

		if @word.update(word_params)
			redirect_to category_path(@word.category_id)
		else
			render 'edit'
		end
	end

	def destroy
		@word = Word.find(params[:id])
		@word.destroy
	 
		redirect_to category_path(@word.category_id)
	end

	private
	  def word_params
	    params.require(:word).permit(:word, :published, :category_id)
	  end
end
