class CategoriesController < ApplicationController
	def new
		@category = Category.new
	end

	def create
		@category = Category.new(category_params)
 		@category.published = false
 		
 		wordsList = params[:category][:words].split(';')
 		wordsList.each do |word|
 			w = Word.new(:word => word, :published => false, :category_id => @category.id )
			@category.words.push(w)
		end

 		require 'digest/md5'
		digest = Digest::MD5.hexdigest(@category.name+@category.description)
		@category.hash_code = digest

 		if @category.save
			redirect_to category_path(@category)
		else
			render 'new'
		end

	end

	def show
	  	@category = Category.find(params[:id]) #where(hash_code: params[:id]).first
	  	@count = @category.words.length
	  	respond_to do |format|
	  		format.html
	  		#format.xml
	  	end
	end

	def index
		@categories = Category.all

		respond_to do |format|
	  		format.html
	  		#format.xml
	  	end
	end

	def edit
	  	@category = Category.find(params[:id])
	end

	def update
		@category = Category.find(params[:id])

		wordsList = params[:category][:words].split(';')
 		wordsList.each do |word|
 			w = Word.new(:word => word, :published => false, :category_id => @category.id )
			@category.words.push(w)
		end

		require 'digest/md5'
		digest = Digest::MD5.hexdigest(category_params[:name]+category_params[:description])
		@category.hash_code = digest

		if @category.update(category_params)
			redirect_to @category
		else
			render 'edit'
		end
	end

	def destroy
		@category = Category.find(params[:id])
		@category.destroy
	 
		redirect_to welcome_index_path
	end

	private
	  def category_params
	    params.require(:category).permit(:name, :description, :published, :instruction)
	  end
end
