class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.string :mode
      t.boolean :published

      t.timestamps
    end
  end
end
