class ChangeStringFormatInCategories < ActiveRecord::Migration
  def change
  	rename_column :categories, :mode, :description
  	change_column :categories, :description, :text
  end
end
