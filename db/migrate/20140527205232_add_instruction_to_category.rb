class AddInstructionToCategory < ActiveRecord::Migration
  def change
    add_column :categories, :instruction, :text
  end
end
