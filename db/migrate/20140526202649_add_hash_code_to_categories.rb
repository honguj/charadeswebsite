class AddHashCodeToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :hash_code, :string
  end
end
